package com.danish.samplecbseguide.di

import com.danish.samplecbseguide.model.CategoryService
import com.danish.samplecbseguide.viewmodel.ListViewModel
import dagger.Component


@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject (service: CategoryService)
    fun inject(viewModel: ListViewModel)
}