package com.danish.samplecbseguide.di

import com.danish.samplecbseguide.model.CategoryApi
import com.danish.samplecbseguide.model.CategoryService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class ApiModule {

    private val BASE_URL = "https://mycbseguide.com/v1/"

    @Provides
    fun provideCategoryApi() : CategoryApi {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(CategoryApi::class.java)
    }

    @Provides
    fun provideCategoryService() : CategoryService {
        return CategoryService()
    }
}