package com.danish.samplecbseguide.model


import com.google.gson.annotations.SerializedName

data class Category(
    @SerializedName("id")
    val id: Int,
    @SerializedName("mobile_logo")
    val mobileLogo: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("parent")
    val parent: Int,
    @SerializedName("weight")
    val weight: Int
)