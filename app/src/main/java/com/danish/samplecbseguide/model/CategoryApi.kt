package com.danish.samplecbseguide.model

import io.reactivex.Single
import retrofit2.http.GET

interface CategoryApi {

    @GET("category/")
    fun getCategory() : Single<Data>
}