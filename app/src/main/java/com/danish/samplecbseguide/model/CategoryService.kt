package com.danish.samplecbseguide.model


import com.danish.samplecbseguide.di.DaggerApiComponent
import io.reactivex.Single
import javax.inject.Inject

class CategoryService {

    @Inject
    lateinit var api: CategoryApi

    init {
        DaggerApiComponent.create().inject(this)
    }

    fun getCategory() : Single<Data>{
        return api.getCategory()
    }
}