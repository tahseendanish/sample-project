package com.danish.samplecbseguide.model


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("categories")
    val categories: List<Category>,
    @SerializedName("status")
    val status: String
)