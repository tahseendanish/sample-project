package com.danish.samplecbseguide.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.danish.samplecbseguide.R
import com.danish.samplecbseguide.model.Category
import kotlinx.android.synthetic.main.cbse_list_item.view.*

class CbseListAdapter(var classes: ArrayList<Category>): RecyclerView.Adapter<CbseListAdapter.CbseViewHolder>() {

    fun updateCbseClasses(newCbseClases : List<Category>){
        classes.clear()
        classes.addAll(newCbseClases)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= CbseViewHolder (
        LayoutInflater.from(parent.context).inflate(R.layout.cbse_list_item,parent,false)
        )

    override fun getItemCount() = classes.size

    override fun onBindViewHolder(holder: CbseViewHolder, position: Int) {
        holder.bind(classes[position])
    }

    class CbseViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        fun bind(category: Category?){
            itemView.tv_id.text = category?.id.toString()
            itemView.tv_name.text = category?.name
            itemView.tv_parent.text = category?.parent.toString()
            itemView.tv_weight.text = category?.weight.toString()
            val movieLogoUrl = category?.mobileLogo
            val options = RequestOptions()
                .placeholder(R.drawable.ic_launcher_background)
                .circleCrop()
                .error(R.mipmap.ic_launcher_round)
            Glide.with(itemView.context)
                .setDefaultRequestOptions(options)
                .load(movieLogoUrl)
                .into(itemView.avatar)
        }

    }

}