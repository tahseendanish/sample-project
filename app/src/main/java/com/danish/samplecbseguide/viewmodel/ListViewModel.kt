package com.danish.samplecbseguide.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danish.samplecbseguide.di.DaggerApiComponent
import com.danish.samplecbseguide.model.Category
import com.danish.samplecbseguide.model.CategoryService
import com.danish.samplecbseguide.model.Data
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class ListViewModel : ViewModel() {

    @Inject
    lateinit var categoryService: CategoryService

    init {
        DaggerApiComponent.create().inject(this)
    }

    private val disposable = CompositeDisposable()

    val categories = MutableLiveData<List<Category>>()
    val categoryLoadError = MutableLiveData<Boolean>()
    val loading = MutableLiveData<Boolean>()

    fun refresh() {
        fetchCategories()
    }

    private fun fetchCategories() {
        loading.value = true
        disposable.add(
            categoryService.getCategory()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<Data>() {
                    override fun onSuccess(data: Data) {
                        Log.d("error ", "" + data)
                        categories.value = data.categories
                        categoryLoadError.value = false
                        loading.value = false
                    }

                    override fun onError(e: Throwable) {
                        categoryLoadError.value = true
                        loading.value = false
                        Log.d("error ", "" + e.printStackTrace())
                    }
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}
